+++
title = "MegerConflict"
date = 2019-05-04T18:30:41+08:00
weight = 5
chapter = true
pre = "<b>X. </b>"
+++

### Chapter X

# MegerConflict
hint: Updates were rejected because the tip of your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
# 解决
勾选强制覆盖已有的分支（可能会丢失改动），再点击上传，上传成功。
~~~
git push -u origin master -f 
~~~
Lorem Ipsum.